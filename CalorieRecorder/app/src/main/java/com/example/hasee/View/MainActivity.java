package com.example.hasee.View;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.hasee.Helper.GreenDaoHelper;
import com.example.hasee.greendao.ItemDao;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.example.hasee.Model.*;

import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.query.QueryBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends AppCompatActivity {

    private TextView click1,click2;
    private Button editButton,queryButton;
    private GraphView graphView;
    private ItemDao itemDao = GreenDaoHelper.getDaoSession().getItemDao();
    private Date beginDate,endDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    public void init()
    {
        click1 = findViewById(R.id.Click1);
        click2 = findViewById(R.id.Click2);
        editButton = findViewById(R.id.EDITbutton);
        queryButton = findViewById(R.id.QUERYbutton);
        graphView =  findViewById(R.id.graph);

    }

    public void setCalendar(final View view)
    {
        Calendar calendar = Calendar.getInstance();
        int nowYear = calendar.get(Calendar.YEAR);
        int nowMonth = calendar.get(Calendar.MONTH);
        int nowDay = calendar.get(Calendar.DATE);

        {
            new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker pview, int setYear, int setMonth, int setDay) {
                   TextView text = findViewById(view.getId());
                   text.setText(setYear+"-"+(setMonth+1)+"-"+setDay);
                   if (text.getId()==click1.getId())
                   {
                       beginDate =  new Date(setYear,(setMonth+1),setDay);
                   }
                   else
                   {
                       endDate = new Date(setYear,(setMonth+1),setDay);
                   }
                }
            },nowYear,nowMonth,nowDay).show();
        }
    }

    public void setEditButton(View view)
    {
        Intent intent = new Intent(MainActivity.this,EditActivity.class);
        startActivity(intent);
    }


    public void setQueryButton(View view)
    {
        List<Item> list =getItemList();
        showGraphView(list);
    }


    public void showGraphView(List<Item> itemList)
    {
        graphView.removeAllSeries();
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
        series.setDrawDataPoints(true);
        for(int i=0;i<itemList.size();i++)
        {
            Item item=itemList.get(i);

      //      double x = Double.parseDouble((item.getYear()-2000)+""+item.getMonth()+""+item.getDay());
            Date x = new Date(item.getYear()-1900,item.getMonth()-1,item.getDay());
          //  Log.d("test1",x.getYear()+"-"+x.getMonth()+"-"+x.getDate());
            double y = item.getCalorie();
         //   Log.d("test2",x+"-"+y);
            series.appendData(new DataPoint(x,y),true,100,true);
        }

        graphView.addSeries(series);

        Viewport viewport =  graphView.getViewport();

        viewport.setScalable(true);
        viewport.setScalableY(true);


        graphView.getGridLabelRenderer().setTextSize(25f);
        graphView.getGridLabelRenderer().reloadStyles();

        DateFormat dateFormat = DateFormat.getDateInstance();
        graphView.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(MainActivity.this,dateFormat));

        graphView.getGridLabelRenderer().setNumHorizontalLabels(3);
        graphView.getGridLabelRenderer().setNumVerticalLabels(4);


    }

    public List<Item> getItemList()
    {
        QueryBuilder<Item> qb= itemDao.queryBuilder();
        qb.where(qb.and(ItemDao.Properties.Year.ge(beginDate.getYear()),
                        ItemDao.Properties.Month.ge(beginDate.getMonth()),
                        ItemDao.Properties.Day.ge(beginDate.getDate())),
                 qb.and(ItemDao.Properties.Year.le(endDate.getYear()),
                         ItemDao.Properties.Month.le(endDate.getMonth()),
                         ItemDao.Properties.Day.le(endDate.getDate()))
        );
        qb.orderAsc(ItemDao.Properties.Year,ItemDao.Properties.Month,ItemDao.Properties.Day);
        return qb.list();
    }

}

