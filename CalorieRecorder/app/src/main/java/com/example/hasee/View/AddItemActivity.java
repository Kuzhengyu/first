package com.example.hasee.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hasee.Helper.GreenDaoHelper;
import com.example.hasee.greendao.ItemDao;
import com.example.hasee.Model.*;
import java.util.Calendar;
import java.util.List;

public class AddItemActivity extends AppCompatActivity {
    private CalendarView calendarView;
    private int setYear,setMonth,setDay;
    private int calorie;
    private EditText calorieText;
    private ItemDao itemDao = GreenDaoHelper.getDaoSession().getItemDao();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        editTextInit();
        calendarInit();
    }

    public void editTextInit()
    {
        calorieText = findViewById(R.id.calorieText);
        calorieText.setHint("Add Calorie here");
    }

    public void calendarInit()
    {
        calendarView = findViewById(R.id.calendarView);
        calendarView.setDate(calendarView.getDate());

        Calendar calendar = Calendar.getInstance();
        setYear = calendar.get(Calendar.YEAR);
        setMonth = calendar.get(Calendar.MONTH)+1;
        setDay = calendar.get(Calendar.DATE);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int day) {
                setYear = year;
                setMonth = month+1;
                setDay = day;
            }
        });
    }

    public void setUpdateButton(View view)
    {
        int result = check();
        if(result==1)
        {
         calorie =  Integer.parseInt(calorieText.getText().toString());
         Item item = new Item (null,setYear,setMonth,setDay,calorie);
         itemDao.insert(item);

     /*    List<Item> itemList = itemDao.loadAll();
         for(int i=0;i<itemList.size();i++)
         {
             Log.d("test",itemList.get(i).getYear()+"");
             Log.d("test",itemList.get(i).getMonth()+"");
             Log.d("test",itemList.get(i).getDay()+"");
             Log.d("test",itemList.get(i).getCalorie()+"");
         }
    */

     //添加完成后关闭窗口并提示
            Toast.makeText(this,"添加成功",Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    private int check()
    {

        return 1;
    }


}
