package com.example.hasee.Model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
    public class Item {

      @Id  private Long id;
        private int year;
        private int month;
        private int day;
        private int calorie;
        @Generated(hash = 1898363595)
        public Item(Long id, int year, int month, int day, int calorie) {
            this.id = id;
            this.year = year;
            this.month = month;
            this.day = day;
            this.calorie = calorie;
        }
        @Generated(hash = 1470900980)
        public Item() {
        }
        public Long getId() {
            return this.id;
        }
        public void setId(Long id) {
            this.id = id;
        }
        public int getYear() {
            return this.year;
        }
        public void setYear(int year) {
            this.year = year;
        }
        public int getMonth() {
            return this.month;
        }
        public void setMonth(int month) {
            this.month = month;
        }
        public int getDay() {
            return this.day;
        }
        public void setDay(int day) {
            this.day = day;
        }
        public int getCalorie() {
            return this.calorie;
        }
        public void setCalorie(int calorie) {
            this.calorie = calorie;
        }
        // for ArrayAdapter
      public String toString(){return year+"-"+month+"-"+day+":"+calorie;}
}
