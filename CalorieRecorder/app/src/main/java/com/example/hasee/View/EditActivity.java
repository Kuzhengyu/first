package com.example.hasee.View;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hasee.Helper.GreenDaoHelper;
import com.example.hasee.Model.*;
import com.example.hasee.greendao.ItemDao;

import java.util.List;

public class EditActivity extends AppCompatActivity {

    private int index;
    private ListView listView;
    private ItemDao itemDao = GreenDaoHelper.getDaoSession().getItemDao();
    private ArrayAdapter<Item> listAdapter;
    private List<Item> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        listView = findViewById(R.id.listView);
        setListView();
    }

    private void setListView() {
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                index =position;
                showDialog();
                return false;
            }
        });
    }

    protected void onResume()
    {
        super.onResume();
        showListView();
    }

    public void setAddButton(View view) {
        Intent intent = new Intent(EditActivity.this,AddItemActivity.class);
        startActivity(intent);
    }

   public void showListView()
    {
        //将数据库内容排序后输出到list列表
       list = itemDao.queryBuilder().orderAsc(ItemDao.Properties.Year,ItemDao.Properties.Month,ItemDao.Properties.Day).list();
       listAdapter =  new ArrayAdapter<Item>(this,android.R.layout.simple_list_item_1,list);
        //        ArrayAdapter会把Item数组每一项以toString输出
       listView.setAdapter(listAdapter);
    }

    public void showDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditActivity.this);
        builder.setTitle("删除");
        builder.setMessage("要删除该项记录吗？");
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            // do nothing
            }
        });
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Item item=list.get(index);
                itemDao.deleteByKey(item.getId());
                showListView();
            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
